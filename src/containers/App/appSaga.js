import { put, takeLatest, all, call, select } from 'redux-saga/effects';
import axios from 'axios';
import { API_BASE } from '../../config';

import { APP_START_INIT } from './constants';
import {
  actionUserGetSuccess,
  actionUsersGetSuccess,
  actionRoomsGetSuccess,
  actionAppFinishInit,
} from './actions';
import { selectUserAuthData } from '../Auth/selectors';

function* handleGetCurrentUser(id) {
  const requestUrl = `${API_BASE}/api/users/${id}`;

  try {
    return yield axios
      .get(requestUrl)
      .then((response) => response.data)
      .catch(() => {});
  } catch (e) {
    console.error(e);
    return {};
  }
}

function* handleGetUsers() {
  const requestUrl = `${API_BASE}/api/users`;

  try {
    return yield axios
      .get(requestUrl)
      .then((response) => response.data)
      .catch(() => []);
  } catch (e) {
    console.error(e);
    return [];
  }
}

function* handleGetRooms() {
  const requestUrl = `${API_BASE}/api/rooms`;

  try {
    return yield axios
      .get(requestUrl)
      .then((response) => response.data)
      .catch(() => []);
  } catch (e) {
    console.error(e);
    return [];
  }
}

function* handleAppStartInit() {
  try {
    const { userId } = yield select(selectUserAuthData);

    const [currentUser, users, rooms] = yield all([
      call(handleGetCurrentUser, userId),
      call(handleGetUsers),
      call(handleGetRooms),
    ]);

    yield put(actionUserGetSuccess({ currentUser }));
    yield put(actionUsersGetSuccess({ users }));
    yield put(actionRoomsGetSuccess({ rooms }));
    yield put(actionAppFinishInit());
  } catch (e) {
    console.error(e);
  }
}

export function* appSaga() {
  yield takeLatest(APP_START_INIT, handleAppStartInit);
}
