import React, { useEffect } from 'react';
import { useSelector, useDispatch, shallowEqual } from 'react-redux';
import { Box } from '@material-ui/core';
import { Switch, Redirect, Route } from 'react-router';
import { push } from 'connected-react-router';
import { useSessionStorage } from 'react-use';

import { useAuthBase } from '../../hooks/useAuthBase';
import { RouteWithCheckAuth } from '../../router/RouteWithCheckAuth';
import { Menu } from '../../components';
import { actionAppLogOut } from '../Auth/actions';
import { People } from '../People/People';
import { Channels } from '../Channels/Channels';
import { About } from '../About/About';
import { selectCurrentUser } from './selectors';
// import index from "../Tic Tac Toe"

export const App = () => {
  const dispatch = useDispatch();
  const { name } = useSelector(selectCurrentUser, shallowEqual);

  const [value, setValue] = useSessionStorage('userAuthData');

  // useEffect(() => {
  //   dispatch(actionAppLogOut());
  //   dispatch(push('/sign-in'));
  // }, [value]);

  return (
    <Box>
      <Menu
        title={name}
        handleLogOut={() => {
          setValue(undefined);
        }}
      >
        <Switch>
          <RouteWithCheckAuth
            exact
            path="/about"
            redirectPath="/sign-in"
            useAuthBase={useAuthBase}
            component={About}
          />
          <RouteWithCheckAuth
            exact
            path="/channels"
            redirectPath="/sign-in"
            useAuthBase={useAuthBase}
            component={Channels}
          />
          <RouteWithCheckAuth
            exact
            path="/people"
            redirectPath="/sign-in"
            useAuthBase={useAuthBase}
            component={People}
          />
          {/* <Route component={}/> */}
          <Route component={() => <Redirect to="/people" />} />
        </Switch>
      </Menu>
    </Box>
  );
};
