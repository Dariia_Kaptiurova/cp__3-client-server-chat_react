module.exports = {
  routing: [
    {
      path: '/people',
      name: 'People',
    },
    {
      path: '/channels',
      name: 'Channels',
    },
    {
      path: '/about',
      name: 'About',
    },
  ],
  users: [
    {
      name: 'Remy Sharp',
      isOnline: true,
      avatar: 'https://material-ui.com/static/images/avatar/1.jpg',
    },
    {
      name: 'Alice',
      isOnline: false,
      avatar: 'https://material-ui.com/static/images/avatar/1.jpg',
    },
    {
      name: 'CindyBaker',
      isOnline: true,
      avatar: 'https://material-ui.com/static/images/avatar/3.jpg',
    },
  ],
  // API_BASE: 'http://localhost:5000',
  API_BASE: 'https://nestjs-chat-api.herokuapp.com',
  // WS_BASE: 'http://localhost:5000',
  WS_BASE: 'https://nestjs-chat-api.herokuapp.com',
};
