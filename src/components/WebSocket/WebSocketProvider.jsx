import React, { createContext } from 'react';
import { useDispatch } from 'react-redux';

import { WS_BASE } from '../../config.js';

export const WebSocketContext = createContext(null);

export const WebSocketProvider = ({ children }) => {
  let socket;
  let ws;

  // const dispatch = useDispatch();

  const sendMessage = (roomId, message) => {
    const payload = {
      roomId,
      data: message,
    };

    socket.send(JSON.stringify(payload));

    // dispatch(sendMessageRequest(payload));
  };

  if (!socket) {
    socket = new WebSocket(WS_BASE);

    // socket.on('event://get-message', (msg) => {
    //   const payload = JSON.parse(msg);
    //   dispatch(updateChatLog(payload));

    // }
    // });

    ws = {
      socket,
      sendMessage,
    };
  }

  return <WebSocketContext.Provider value={ws}>{children}</WebSocketContext.Provider>;
};
