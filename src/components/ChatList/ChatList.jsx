import React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

import { BadgeAvatar } from '../BadgeAvatar/BadgeAvatar';

export const ChatList = (props) => {
  const { items, handleChangeRoom } = props;
  return (
    <div>
      <List>
        {items.map((item, index) => {
          return (
            <ListItem key={`${index + 1}`} button onClick={() => handleChangeRoom(item._id)}>
              <ListItemIcon>
                <BadgeAvatar
                  src={item.avatar | ''}
                  alt={item.name}
                  isOnline={item.isOnline || false}
                />
              </ListItemIcon>
              <ListItemText primary={item.name}>{item.name}</ListItemText>
            </ListItem>
          );
        })}
      </List>
    </div>
  );
};
